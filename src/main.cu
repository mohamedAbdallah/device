#include<device.h>

int main()
{
    int numDevices;
    numDevices = Spectra::CUDA::Device::getDeviceCount();
//    LOG_INFO("Number of Devices= %d",numDevices);
    for( int i = 0; i < numDevices; i++ )
    {
        Spectra::CUDA::Device *myDevices = new Spectra::CUDA::Device( i );
//        std::cout << myDevices->getName() << std::endl;
        myDevices->printSpecs();
    }

    return EXIT_SUCCESS;
}
