#ifndef DEVICE_H
#define DEVICE_H

//#include<stdio.h>
#include<stdlib.h>
#include<sstream>
#include<iostream>
#include<iomanip>
#include<logger/Logger.h>
#include<vector>
#include<typeinfo>
#include<string>

#include<cuda.h>
#include<cuda_runtime.h>
#include<cuda_runtime_api.h>
#include<helper_cuda.h>
/**
 *
 */
namespace Spectra
{
/**
 *
 */
namespace CUDA
{
/**
 * @brief The Device class
 */
class Device
{
public:
    /**
     * @brief Device
     */
    Device( uint deviceID );

    /**
     * @brief getDeviceCount
     * @return
     */
    static uint getDeviceCount( void );

    /**
     * @brief printSpecs
     */
    void printSpecs( void );

    uint ID( void ) const;

    std::string name( void ) const;

    /**
     * @brief getNumberCores
     * @return
     */
    uint numberCores( void ) const;

    uint numberSharedMemPerMP( void ) const;


    uint totalMemorySize( void ) const;

    /**
     * @brief getAvailableMemorySize
     * @return
     */
    uint availableMemorySize( void ) const;

    /**
     * @brief CUDACapability
     * @return
     */
    float CUDACapability( void ) const;

    /**
     * @brief corePerMultiProc
     * @return
     */
    uint corePerMultiProc( void ) const;

    /**
     * @brief GPUclockRate
     * @return
     */
    uint GPUclockRate( void ) const;
    /**
     * @brief memClockRate
     * @return
     */
    uint memClockRate( void ) const;

    /**
     * @brief l2CacheSize
     * @return
     */
    uint l2CacheSize ( void ) const;

    /**
      This member function returns the maximum Texture dimensions.
      1D first element, 2D second and third elements, 3D forth, fifth and sixth elements
     * @brief maxTextureDimSizes
     * @return
     */
    uint* maxTextureDimSizes( void ) const;

    /**
     * @brief maxLayer1DTextureSizeAndNumlayers
     * @return
     */
    int *maxLayer1DTextureSizeAndNumlayers( void );

    /**
     * @brief maxLayer2DTextureSizeAndNumlayers
     * @return
     */
    int *maxLayer2DTextureSizeAndNumlayers( void ) ;

    /**
     * @brief constMemory
     * @return
     */
    uint constMemory( void ) const;

    /**
     * @brief sharedMemPerBlock
     * @return
     */
    uint sharedMemPerBlock( void ) const;

    /**
     * @brief memoryBusWidth
     * @return
     */
    uint memoryBusWidth( void ) const;

    /**
     * @brief numRegPerBlock
     * @return
     */
    uint numRegPerBlock( void ) const;

    /**
     * @brief warpSize
     * @return
     */
    uint warpSize( void ) const;

    /**
     * @brief maxNumThreadPerMP
     * @return
     */
    uint maxNumThreadPerMP( void ) const;

    /**
     * @brief maxNumThreadPerBlock
     * @return
     */
    uint maxNumThreadPerBlock( void ) const;

    /**
     * @brief maxDimSizeThreadBlock
     * @return
     */
    int *maxDimSizeThreadBlock( void );

    /**
     * @brief maxdimSizeGridSize
     * @return
     */
    int* maxdimSizeGridSize( void );

    /**
     * @brief textureAlignment
     * @return
     */
    uint textureAlignment( void ) const;

    /**
     * @brief maxMemoryPitch
     * @return
     */
    uint maxMemoryPitch( void ) const;

    /**
     * @brief concurrentCopyAndKernelExecution
     * @return
     */
    bool concurrentCopyAndKernelExecution( void ) const;

    /**
     * @brief runTimeLimitOnKernels
     * @return
     */
    bool runTimeLimitOnKernels( void ) const;

    /**
     * @brief integratedGPUSharingHostMemory
     * @return
     */
    bool integratedGPUSharingHostMemory( void ) const;

    /**
     * @brief supportHostPageLockedMemoryMapping
     * @return
     */
    bool supportHostPageLockedMemoryMapping( void ) const;

    /**
     * @brief concurrentKernelExecution
     * @return
     */
    bool concurrentKernelExecution( void ) const;

    /**
     * @brief alignmentReqForSurfaces
     * @return
     */
    bool alignmentReqForSurfaces( void ) const;
    /**
     * @brief ECCsupport
     * @return
     */
    bool ECCsupport( void ) const;

    /**
     * @brief computeMode
     * @return
     */
    int computeMode( void ) const;
//    std::string computeMode( void ) const;

    /**
     * @brief PCIDeviceID
     * @return
     */
    uint PCIDeviceID( void ) const;

    /**
     * @brief PCIBusID
     * @return
     */
    uint PCIBusID( void ) const;

    /**
     * @brief PCIDomainID
     * @return
     */
    uint PCIDomainID( void ) const;

    /**
     * @brief supportUnifiedAddressing
     * @return
     */
    bool supportUnifiedAddressing( void ) const;
private:

    /**
     * @brief querySpecs
     */
    void querySpecs();

    /**
     * @brief printError
     */
    void printError( void ) const;

    /**
     * @brief prop2String
     */
    void prop2String( void );

    /**
     * @brief formatingOutput
     */
    void formatingOutput ( void );

    cudaError_t error_;

    /**
     * @brief deviceCount_
     */
    int deviceCount_;

    /**
     * @brief deviceID_
     */
    uint deviceID_;

    /**
     * @brief properties_
     */
    cudaDeviceProp properties_;

    /**
     * @brief outputStream_
     */
    std::ostringstream outputStream_;

    /**
     * @brief attributes_
     */
    std::vector<std::string> attributes_;

    /**
     * @brief propValue_
     */
    std::vector<std::string> propValue_;
};
}
}

#endif // DEVICE_H
