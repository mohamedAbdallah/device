#include "device.h"

Spectra::CUDA::Device::Device( uint deviceID )
{
    deviceID_ = deviceID;
    this->querySpecs();
}

uint Spectra::CUDA::Device::getDeviceCount(void )
{
    cudaError_t error;
    int deviceCount;
    error = cudaGetDeviceCount( &deviceCount );
    return deviceCount;
}

void Spectra::CUDA::Device::printSpecs()
{
    LOG_INFO("Querying device specifications*******");
    std::cout << std::setfill( '*' ) << std::setw(80) << "\n";
    std::cout << std::setfill(' ') << std::setw(40) << "Device #" << deviceID_ << std::endl;
    std::cout << std::setw(40) << this->name() << "\n";
    this->prop2String();
    this->formatingOutput();
}

uint Spectra::CUDA::Device::ID() const
{
    return deviceID_;
}

std::string Spectra::CUDA::Device::name() const
{
    std::string nameString = "";
    nameString += properties_.name;
    return nameString;
}

uint Spectra::CUDA::Device::numberCores() const
{
    return properties_.multiProcessorCount;
}

uint Spectra::CUDA::Device::numberSharedMemPerMP() const
{
    return properties_.sharedMemPerMultiprocessor;
}

uint Spectra::CUDA::Device::totalMemorySize() const
{
    return properties_.totalGlobalMem;
}

uint Spectra::CUDA::Device::availableMemorySize() const
{
    return properties_.canMapHostMemory;
}

float Spectra::CUDA::Device::CUDACapability() const
{
    return properties_.major + properties_.minor / 10;
}

uint Spectra::CUDA::Device::corePerMultiProc() const
{
    return _ConvertSMVer2Cores(
                properties_.major,
                properties_.minor );
}

uint Spectra::CUDA::Device::GPUclockRate() const
{
    return properties_.clockRate;
}

uint Spectra::CUDA::Device::memClockRate() const
{
    return properties_.memoryClockRate;
}

uint Spectra::CUDA::Device::l2CacheSize() const
{
    return properties_.l2CacheSize;
}

uint *Spectra::CUDA::Device::maxTextureDimSizes() const
{
    uint* textureDimenstions;
    textureDimenstions = new uint[6];
    textureDimenstions[ 0 ] = properties_.maxTexture1D;
    textureDimenstions[ 1 ] = properties_.maxTexture2D[0];
    textureDimenstions[ 2 ] = properties_.maxTexture2D[1];
    textureDimenstions[ 3 ] = properties_.maxTexture3D[0];
    textureDimenstions[ 4 ] = properties_.maxTexture3D[1];
    textureDimenstions[ 5 ] = properties_.maxTexture3D[2];
    return textureDimenstions;
}

int *Spectra::CUDA::Device::maxLayer1DTextureSizeAndNumlayers()
{
    return properties_.maxTexture1DLayered;
}

int *Spectra::CUDA::Device::maxLayer2DTextureSizeAndNumlayers()
{
    return properties_.maxSurface2DLayered;
}

uint Spectra::CUDA::Device::constMemory() const
{
    return properties_.totalConstMem;
}

uint Spectra::CUDA::Device::sharedMemPerBlock() const
{
    return properties_.sharedMemPerBlock;
}

uint Spectra::CUDA::Device::memoryBusWidth() const
{
    return properties_.memoryBusWidth;
}

uint Spectra::CUDA::Device::numRegPerBlock() const
{
    return properties_.regsPerBlock;
}

uint Spectra::CUDA::Device::warpSize() const
{
    return properties_.warpSize;
}

uint Spectra::CUDA::Device::maxNumThreadPerMP() const
{
    return properties_.maxThreadsPerMultiProcessor;
}

uint Spectra::CUDA::Device::maxNumThreadPerBlock() const
{
    return properties_.maxThreadsPerBlock;
}

int *Spectra::CUDA::Device::maxDimSizeThreadBlock()
{
    return properties_.maxThreadsDim;
}

uint Spectra::CUDA::Device::textureAlignment() const
{
    return properties_.textureAlignment;
}

uint Spectra::CUDA::Device::maxMemoryPitch() const
{
    return properties_.memPitch;
}

bool Spectra::CUDA::Device::concurrentCopyAndKernelExecution() const
{
    return properties_.deviceOverlap;
}

bool Spectra::CUDA::Device::runTimeLimitOnKernels() const
{
    properties_.kernelExecTimeoutEnabled;
}

bool Spectra::CUDA::Device::integratedGPUSharingHostMemory() const
{
    properties_.integrated;
}

bool Spectra::CUDA::Device::supportHostPageLockedMemoryMapping() const
{
    return properties_.canMapHostMemory;
}

bool Spectra::CUDA::Device::concurrentKernelExecution() const
{
    return properties_.concurrentKernels;
}

bool Spectra::CUDA::Device::alignmentReqForSurfaces() const
{
    return properties_.surfaceAlignment;
}

bool Spectra::CUDA::Device::ECCsupport() const
{
    return properties_.ECCEnabled;
}

int Spectra::CUDA::Device::computeMode() const
{
    return properties_.computeMode;
}

//std::string Spectra::CUDA::Device::computeMode() const
//{
//    std::vector<std::string> mode;
//    mode.push_back( "Default mode: Device is not restricted and multiple threads"
//                    " can use cudaSetDevice() with this device" );
//    mode.push_back( "Exclusive mode: Only one thread will be able to use "
//                    "cudaSetDevice() with this device." );
//    mode.push_back( "Prohibited mode: No threads can use cudaSetDevice() with"
//                    " this device. Any errors from calling cudaSetDevice() with"
//                    " an exclusive (and occupied) or prohibited device will only"
//                    " show up after a non-device management runtime function is "
//                    "called. At that time, cudaErrorNoDevice will be returned." );
//    return mode[ computeMode() ];
//}

uint Spectra::CUDA::Device::PCIDeviceID() const
{
    return properties_.pciDeviceID;
}

uint Spectra::CUDA::Device::PCIBusID() const
{
    return properties_.pciBusID;
}

uint Spectra::CUDA::Device::PCIDomainID() const
{
    return properties_.pciDomainID;
}

bool Spectra::CUDA::Device::supportUnifiedAddressing() const
{
    return properties_.unifiedAddressing;
}

void Spectra::CUDA::Device::querySpecs()
{
    LOG_INFO("LOGGING Device specifications");
    error_ = cudaGetDeviceProperties( &properties_, deviceID_ );
    this->printError();

}

void Spectra::CUDA::Device::printError() const
{
    if ( error_ != cudaSuccess )
    {
        std::string errorText = "";
        errorText = "CUDA RUNTIME ERROR******";
        errorText += cudaGetErrorString( error_ );
        std::cout << errorText;
    }
}

void Spectra::CUDA::Device::prop2String( void )
{

    attributes_.push_back( "Device Name:" );
    propValue_.push_back( name() );



    attributes_.push_back( "CUDA Capability version number:" );
    propValue_.push_back( std::to_string( CUDACapability()));

    attributes_.push_back( "# of Multiprocessors(MP), # of CUDA core/MP : " );
    propValue_.push_back( std::to_string( numberCores() )
                                         + ", " +
                          std::to_string( corePerMultiProc() )
                          + "( total " + std::to_string(
                              numberCores() * corePerMultiProc())
                          + " CUDA core )");

    attributes_.push_back ( "Clock rate:" );
    propValue_.push_back ( std::to_string( properties_.clockRate / 1000 ) +
                           " MHz" );

    attributes_.push_back( "Memory clock: " );
    propValue_.push_back( std::to_string( memClockRate() / 1000 ) + " MHz" );

    attributes_.push_back("Total global memory:");
    propValue_.push_back( std::to_string( totalMemorySize() / (1024*1024) )+
                          " MB(" + std::to_string(  totalMemorySize() ) +
                          " byte)");

    attributes_.push_back ( "Total constant memory:" );
    propValue_.push_back ( std::to_string ( constMemory() ));

    attributes_.push_back( "Mermory bus width" );
    propValue_.push_back( std::to_string( memoryBusWidth() ) );

    attributes_.push_back( "L2 Cache Size: " );
    propValue_.push_back( std::to_string( l2CacheSize() ) );

    attributes_.push_back("Total shared memory per block:");
    propValue_.push_back( std::to_string( sharedMemPerBlock() / 1024 )
                          + " MB");


    attributes_.push_back ( "Total registers per block:" );
    propValue_.push_back ( std::to_string( numRegPerBlock() / 1024 )
                           + " MB");

    attributes_.push_back ( "Warp size:" );
    propValue_.push_back ( std::to_string( properties_.warpSize ));

    attributes_.push_back ( "Maximum memory pitch: " );
    propValue_.push_back ( std::to_string( properties_.memPitch / ( 1024*1024 )) +
                           " MB");


    attributes_.push_back ( "Maximum threads per block:" );
    propValue_.push_back ( std::to_string( properties_.maxThreadsPerBlock ));

    attributes_.push_back ( "Maximum dimension of block:" );
    propValue_.push_back ( std::to_string( properties_.maxThreadsDim[0] ) + " X "
            + std::to_string( properties_.maxThreadsDim[1] )
            +" X " + std::to_string( properties_.maxThreadsDim[2] ));

    attributes_.push_back ( "Maximum dimension of grid:" );
    propValue_.push_back ( std::to_string( properties_.maxGridSize[0] ) + " X "
            + std::to_string( properties_.maxGridSize[1] ) + " X "
            + std::to_string( properties_.maxGridSize[2] ));

    attributes_.push_back( "Maximum texture dimension size: " );
    propValue_.push_back( "1D ( " + std::to_string( maxTextureDimSizes()[0] ) +
            " ), 2D ( " + std::to_string( maxTextureDimSizes()[1] ) + ", "
            + std::to_string( maxTextureDimSizes()[2] ) + " ), 3D ( " +
            std::to_string( maxTextureDimSizes()[3] ) + ", "
            + std::to_string( maxTextureDimSizes()[4] ) + "," +
            std::to_string( maxTextureDimSizes()[5] ) + " )" );

    attributes_.push_back( "Maximum Layered 1D Texture Size, # of layers: " );
    propValue_.push_back( "Size: " +
                          std::to_string( maxLayer1DTextureSizeAndNumlayers()[0] )
                         + ", # of layers: " +
                          std::to_string( maxLayer1DTextureSizeAndNumlayers()[1] ));

    attributes_.push_back( "Maximum Layered 2D Texture Size, # of layers: " );
    propValue_.push_back( "Size: ( " +
                          std::to_string( maxLayer2DTextureSizeAndNumlayers()[0])
                          +  ", " +
                          std::to_string( maxLayer2DTextureSizeAndNumlayers()[1] )
                          + " ), # of layers: " +
                          std::to_string( maxLayer2DTextureSizeAndNumlayers()[2] ));

    attributes_.push_back ( "Texture alignment:" );
    propValue_.push_back ( std::to_string( properties_.textureAlignment ));

    attributes_.push_back ( "Concurrent copy and execution: " );
    propValue_.push_back ( ( concurrentCopyAndKernelExecution() ? "Yes" : "No" ));

    attributes_.push_back ( "Concurrent kernel execution: " );
    propValue_.push_back ( ( concurrentKernelExecution() ? "Yes" : "No" ));

    attributes_.push_back ( "Kernel execution timeout:" );
    propValue_.push_back (( this->runTimeLimitOnKernels() ? "Yes" : "No" ));

    attributes_.push_back( "Integrated GPU sharing Host Memory: " );
    propValue_.push_back( ( integratedGPUSharingHostMemory() ? "Yes" : "No" ));

    attributes_.push_back( "Support host page-locked memory mapping: " );
    propValue_.push_back( ( supportHostPageLockedMemoryMapping() ? "Yes" : "No" ));

    attributes_.push_back( "Alignment required for Surfaces: " );
    propValue_.push_back( ( alignmentReqForSurfaces() ? "Yes" : "No" ));

    attributes_.push_back( "ECC support: " );
    propValue_.push_back( ( ECCsupport() ? "Yes" : "No" ));

    attributes_.push_back( "Supports Unified Addressing (UVA): " );
    propValue_.push_back( ( this->supportUnifiedAddressing() ? "Yes" : "No" ));

    attributes_.push_back( "PCI Domain ID, Bus ID, location ID: ");
    propValue_.push_back( std::to_string( PCIDomainID() ) + ", " +
                          std::to_string( PCIBusID() ) + ", " +
                          std::to_string( PCIDomainID() ));


    if ( attributes_.size() != propValue_.size() )
        LOG_EXIT("# of attributes not equal to # the properties");

}

void Spectra::CUDA::Device::formatingOutput()
{
    size_t maxAttribSize = 0;

    for ( int itr = 0; itr < attributes_.size(); itr++ )
    {
        std::string tempAttrib = attributes_[ itr ];
        if ( tempAttrib.size() > maxAttribSize  )
            maxAttribSize = tempAttrib.size();
    }
    for ( int itr = 0; itr < attributes_.size(); itr++ )
    {
        outputStream_ << std::setw( maxAttribSize ) << std::left
                      << attributes_[ itr ] << std::setw( 80 - maxAttribSize )
                      << propValue_[itr] << "\n";
    }

    std::cout << outputStream_.str();

}

